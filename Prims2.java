import java.util.*;
import java.io.*;

public class Prims2 {
	Set<Node> visitedNodes = new HashSet();
	Set<Edge> minEdges = new HashSet();

//declares sets of old nodes
	Set<Node> oldNodes;
	Set<Edge> oldEdges;


//creates a new Prims object
	public void Prims2() {
	}

//function to add a node to the list of visited nodes
	public void addVistedNode(Node node) {
		visitedNodes.add(node);
	}

//function to add an edge to the list of minimum edges
	public void addMinEdge(Edge edge) {
		minEdges.add(edge);
	}

//function to set old edges and nodes
	public void setOldEdges (Graph graph) {
		oldNodes = graph.nodes();
		oldEdges = graph.edges();
	}

//function to find connected edges from a node
	public Set<Edge> findConnectedEdges(Node node, Set<Edge> edgeSet) {
		Set<Edge> parsingEdges = new HashSet();
		String passedNode = node.toString();
		for (Edge edge : edgeSet) {
			String firstNodeString = edge.id1();
			if (passedNode.equals(firstNodeString)) {
				parsingEdges.add(edge);
			}

		}
		return parsingEdges;
	}

//function to find the minimum weight from a set of edges
	public Edge findMinimumWeight(Set<Edge> edgeSet) {
		Edge minEdge = new Edge();
		double minWeight = 0;
		for (Edge edge : edgeSet) {
			minWeight = edge.weight();
		}
		for (Edge edge : edgeSet) {
			if (edge.weight() < minWeight) {
				minWeight = edge.weight();
				minEdge = edge;
			}
			else if (edge.weight() == minWeight) {
				minEdge = edge;
			}
		}
		return minEdge;
	}

//function to get a first node
	public Node getFirstNode(Set<Node> nodeSet) {
		Node firstNode = new Node("");
		for (Node node : nodeSet) {
			firstNode = node;
		}
		return firstNode;
	}

//function to add minimum node to node set and returns the next node
	public Node addNode(Edge edge) {
		String node = edge.id2();
			// if (!visitedNodes.contains(edge.id1())) {
			// 	String node1 = edge.id1();
			// 	Node firstNode = new Node(node1);
			// 	visitedNodes.add(firstNode);
			// }
		Node newNode = new Node(node);
		visitedNodes.add(newNode);
		return newNode;
	}

//function to iterate through tree
	public Node prodSpanTree(Node firstNode) {
		Set<Edge> parsingEdges = findConnectedEdges(firstNode, oldEdges);
		Edge minEdge = findMinimumWeight(parsingEdges);
		minEdges.add(minEdge);
		firstNode = addNode(minEdge);
		return firstNode;
	}

//function to produce a mst
	public void prodMST() {
		Node firstNode = getFirstNode(oldNodes);
		visitedNodes.add(firstNode)
		while (visitedNodes != oldNodes) {
			firstNode = prodSpanTree(firstNode);
			oldNodes.remove(firstNode);
		}
	}

//function to produce a graph with minimum spanning tree
	public Graph produceGraph(Graph graph) {
		Graph mst = new Graph();
		oldNodes = graph.nodes();
		oldEdges = graph.edges();
		prodMST();
		for (Node node : visitedNodes) {
			mst.add(node);
		}
		for (Edge edge : minEdges) {
			mst.add(edge);
			System.out.println(edge);
		}
		return mst;

	}

}