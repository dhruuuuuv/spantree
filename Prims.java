import java.util.*;
import java.io.*;

class Prims {
	Set<Edge> usedEdges = new HashSet();
	Set<Node> visitedNodes = new HashSet();
	Set<Edge> oldEdges = new HashSet();
	Set<Node> oldNodes = new HashSet();
	int numberNodes = 0;
	Graph graph;
	Graph mst = new Graph();
	int i = 0;

//constructor for Prims object
	public Prims(Graph graph) {
		this.graph = graph;
		oldEdges = graph.edges();
		oldNodes = graph.nodes();
		numberNodes = oldNodes.size();
	}

//set first node of the graph and add it to the set of visited nodes
	public Node getFirstNode() {
		Node firstNode = null;
		for (Node node : oldNodes) {
			firstNode = node;
			break;
		}
		addNode(firstNode);
		return firstNode;
	}

//adds node to the set of visited nodes
	public void addNode(Node node) {
		visitedNodes.add(node);
	}

//adds node to the set of visited nodes
	public void addEdge(Edge edge) {
		usedEdges.add(edge);
	}

//find the minimum edge weight from a set of edges and nodes
	public Edge findMinimumWeight(Set<Node> nodes, Set<Edge> edges) {
		Edge minEdge = new Edge();
		double minWeight = 100000;
		for (Edge edge : edges) {
			Node u = new Node(edge.id1());
			Node v = new Node(edge.id2());
			if ((edge.weight() < minWeight) && !(contains(usedEdges, edge)) ) {
				minWeight = edge.weight();
				minEdge = edge;
			}
		}
		//System.out.println(visitedNodes);
		return minEdge;
	}

//function to add second node to the visited nodes
	public void addNextNode(Edge minEdge) {
		Node u = new Node(minEdge.id1());
		Node v = new Node(minEdge.id2());
	if (contains(visitedNodes, v))
		visitedNodes.add(u);
	else
		visitedNodes.add(v);
	}

//function to add the nodes and edges to the graph object
	public void completeGraph() {
		for (Node node : visitedNodes) {
			mst.add(node);
		}
		for (Edge edge : usedEdges) {
			mst.add(edge);
		}
	}
 
 	public boolean contains(Set<Node> nodes, Node node) {
 		for (Node n : nodes) {
 			if (n.name().equals(node.name())) {
 				return true;
 			}

 		}
 		return false;
 	}

 	public boolean contains(Set<Edge> edges, Edge edge) {
 		for (Edge e : edges) {
 			if (e.toString().equals(edge.toString())) {
	 				return true;
 			}
 		}
 		return false;
 	}

//main function
	public Graph makeSpanningTree() {
		while(visitedNodes.size() <= oldNodes.size()) {
			Node firstNode = getFirstNode();
			// write("firstNode:"+firstNode);
			Set<Edge> connectedEdges = graph.findConnectedEdges(visitedNodes);
			// write("visited nodes:"+visitedNodes);
			Edge minEdge = findMinimumWeight(visitedNodes, connectedEdges);
			// write("minEdge: "+usedEdges);
			// takeyourtime();
			addNextNode(minEdge);
			usedEdges.add(minEdge);
			// System.out.println(visitedNodes);
			// System.out.println(usedEdges);

		}
		// System.out.println(visitedNodes.size());
		completeGraph();
		return mst;
	}
	public void write(String s)
	{
		System.out.println(s);
	}
	public void takeyourtime()
	{
		Scanner s = new Scanner(System.in);
		s.next();
	}
}