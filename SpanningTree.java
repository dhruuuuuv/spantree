import java.util.*;
import java.io.*;
import java.text.SimpleDateFormat;

public class SpanningTree {

	public static void main(String[] args) {
		try {
			SpanningTree st = new SpanningTree();
			String arg1 = args[0];
			String arg2 = args[1];
			Reader in = new Reader();
			in.read(arg2);
			Graph graph = in.graph();

			switch(arg1) {
				case "-p1" :
				st.test1(graph);
				break;

				case "-p2" :
				st.test2(graph);
				break;

				case "-p3" :
				st.test3(graph);
			}
		}
		catch (IOException e0) {
			System.err.println("Cannot parse graph correctly.");
			System.exit(1);
		}
	}

//function to calculate the total amount of cable needed to connect graph
	public void test1 (Graph graph) {
			double cablen;
			cablen = getTotal(graph) * 1000;
			System.out.println("Total Cable Needed: " + roundTwoDec(cablen) + 'm');

	}

//function to sum up the edge weights in a graph
	public double getTotal (Graph graphtotal) {
		double sum = 0;
		for (Edge edge : graphtotal.edges()) {
			sum = sum + edge.weight();
		}
			return sum;
	}

	public String roundTwoDec(double num){
		String str = String.format("%.2f", num);
		return str;
	}

//function to print the output for each case given
	public void test2 (Graph graph) { 
		double cost = getTotal(prodGovGraph(graph));
		System.out.println("Price: " + roundTwoDec(cost));
		double hours = getTotal(prodComGraph(graph));
		System.out.println("Hours of Disrupted Travel: " + roundTwoDec(hours) + 'h');
		double mins = getTotal(prodSciGraph(graph));
		printDate(mins);	
	}

//function to produce the output with a MST instead of the given graph
	public void test3 (Graph graph) {
		Graph gov = prodGovGraph(graph);
		Graph com = prodComGraph(graph);
		Graph sci = prodSciGraph(graph);
		Prims primsgov = new Prims(gov);
		Prims primscom = new Prims(com);
		Prims primssci = new Prims(sci);
		// Graph mst = prims.makeSpanningTree();
		gov = primsgov.makeSpanningTree();
		com = primscom.makeSpanningTree();
		sci = primssci.makeSpanningTree();
		double cost = getTotal(gov);
		System.out.println("Price: " + roundTwoDec(cost));
		double hours = getTotal(com);
		System.out.println("Hours of Disrupted Travel: " + roundTwoDec(hours) + 'h');
		double mins = getTotal(sci);
		printDate(mins);	
		try {
		Writer out = new Writer();
		out.write("primsgov.txt", gov);
		out.write("primscom.txt", com);
		out.write("primssci.txt", sci);
		}
		catch (IOException e0) {
			System.out.println("Output Error");
			System.exit(1);
		}
	}
//function to produce a graph with the edges weighted for costs
	public Graph prodGovGraph (Graph graph) {
		Graph govGraph = new Graph();
		Set<Edge> govEdges = graph.edges();
		for (Edge edge : govEdges) {
			if (edge.type() == Edge.EdgeType.LocalRoad) {
				double newEdgeWeight;
				newEdgeWeight = 5000 + (edge.weight()*4500);
				Edge newEdge = new Edge(edge.id1(),edge.id2(),newEdgeWeight,edge.type());
				edge = newEdge;
			}

			else if (edge.type() == Edge.EdgeType.MainRoad) {
				double newEdgeWeight;
				newEdgeWeight = (edge.weight()*4000);
				Edge newEdge = new Edge(edge.id1(),edge.id2(),newEdgeWeight,edge.type());
				edge = newEdge;
				
			}

			else if (edge.type() == Edge.EdgeType.Underground) {
				double newEdgeWeight;
				newEdgeWeight = (edge.weight()*1000);
				Edge newEdge = new Edge(edge.id1(),edge.id2(),newEdgeWeight,edge.type());
				edge = newEdge;
			}
			govGraph.add(edge);
		}

		Set<Node> govNodes = graph.nodes();
		for (Node node : govNodes) {
			govGraph.add(node);
		}
		return govGraph;
	}

//function to produce a graph with edges weighted for hours
	public Graph prodComGraph (Graph graph) {
		Graph comGraph = new Graph();
		Set<Edge> comEdges = graph.edges();
		for (Edge edge : comEdges) {
			if (edge.type() == Edge.EdgeType.LocalRoad) {
				double newEdgeWeight;
				newEdgeWeight = (edge.weight()*0.2);
				Edge newEdge = new Edge(edge.id1(),edge.id2(),newEdgeWeight,edge.type());
				edge = newEdge;
			}

			else if (edge.type() == Edge.EdgeType.MainRoad) {
				double newEdgeWeight;
				newEdgeWeight = (edge.weight()*0.5);
				Edge newEdge = new Edge(edge.id1(),edge.id2(),newEdgeWeight,edge.type());
				edge = newEdge;
				
			}

			else if (edge.type() == Edge.EdgeType.Underground) {
				double newEdgeWeight;
				newEdgeWeight = (edge.weight()*1.0);
				Edge newEdge = new Edge(edge.id1(),edge.id2(),newEdgeWeight,edge.type());
				edge = newEdge;
			}
			comGraph.add(edge);
		}

		Set<Node> comNodes = graph.nodes();
		for (Node node : comNodes) {
			comGraph.add(node);
		}
		return comGraph;
	}

//function to produce a graph with edges weighted for time in minutes
		public Graph prodSciGraph (Graph graph) {
			Graph sciGraph = new Graph();
			Set<Edge> sciEdges = graph.edges();
			for (Edge edge : sciEdges) {
				if (edge.type() == Edge.EdgeType.LocalRoad) {
					double newEdgeWeight;
					newEdgeWeight = ((edge.weight()*24*60)/0.2);
					Edge newEdge = new Edge(edge.id1(),edge.id2(),newEdgeWeight,edge.type());
					edge = newEdge;
				}

				else if (edge.type() == Edge.EdgeType.MainRoad) {
					double newEdgeWeight;
					newEdgeWeight = ((edge.weight()*24*60)/0.6);
					Edge newEdge = new Edge(edge.id1(),edge.id2(),newEdgeWeight,edge.type());
					edge = newEdge;
					
				}

				else if (edge.type() == Edge.EdgeType.Underground) {
					double newEdgeWeight;
					newEdgeWeight = ((edge.weight()*24*60)/0.9);
					Edge newEdge = new Edge(edge.id1(),edge.id2(),newEdgeWeight,edge.type());
					edge = newEdge;
				}
				sciGraph.add(edge);
			}

			Set<Node> sciNodes = graph.nodes();
			for (Node node : sciNodes) {
				sciGraph.add(node);
			}
			return sciGraph;
		}

//function to compute the date of the cable laying completion
		public void printDate (double mins) {
				int minutes = (int)(mins);
				Calendar calendar = new GregorianCalendar(2014,1,15,00,00);
				calendar.add(Calendar.MINUTE, minutes);
				SimpleDateFormat sdf = new SimpleDateFormat("EEE d MMMMM yyyy HH:mm");
				String date = sdf.format(calendar.getTime());
				System.out.println("Completion Date: " + date);
		}
}